#include "pch.h"
#include "framework.h"
#include "GRAF.h"
#include "GRAFDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CGRAFDlg::CGRAFDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_GRAF_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CGRAFDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CGRAFDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
END_MESSAGE_MAP()

BOOL CGRAFDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	//graf.citire...;
	graf.SetNoOfNodes(10);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CGRAFDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	dc.MoveTo(20, 100);
	dc.LineTo(200, 200);
	int i = 5;
	char s[10];
	sprintf_s(s, "%d", graf.GetNoOfNodes());
	int x, y, x1, y1;
	x = 10; y = 90;
	x1 = x + 30;
	y1 = y + 30;
	dc.Ellipse(x, y, x1, y1);
	dc.TextOutW(x + 5, y + 5, s);
	//dc.TextOut(x+5, y+5, s);
	x = 175; y = 180;
	x1 = x + 30;
	y1 = y + 30;
	dc.Ellipse(x, y, x1, y1);
	//dc.TextOut(x + 5, y + 5, s);
}


HCURSOR CGRAFDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

