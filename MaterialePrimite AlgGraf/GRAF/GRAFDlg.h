#pragma once

class Graf
{
	int n;
public:
	void SetNoOfNodes(int n)
	{
		this->n = n;
	}
	int GetNoOfNodes()
	{
		return n;
	}
};
class CGRAFDlg : public CDialogEx
{
// Construction
public:
	CGRAFDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GRAF_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
protected:
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	Graf graf;
};
