package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private int nodeNr = 1;
	private int node_diam = 30;
	private Vector<Node> vectorNoduri;
	private Vector<Arc> vectorArce;

	public MyPanel(int[][] matrix) {
		
		vectorNoduri = new Vector<Node>();
		vectorArce = new Vector<Arc>();

		setBorder(BorderFactory.createLineBorder(Color.black));

		for (int index = 0; index < matrix.length; ++index) {
			addNode(getRandomNumber(0, 400), getRandomNumber(0, 400));
		}

		for (int indexRow = 0; indexRow < matrix.length; ++indexRow) {
			for (int indexColumn = 0; indexColumn < matrix.length; ++indexColumn) {
				if (matrix[indexRow][indexColumn] == 1) {
					
					Node first = vectorNoduri.elementAt(indexRow);
					Node second = vectorNoduri.elementAt(indexColumn);
					Point start = new Point(first.getCoordX() + node_diam / 2, first.getCoordY() + node_diam / 2);
					Point finish = new Point(second.getCoordX() + node_diam / 2, second.getCoordY() + node_diam / 2);
					addArc(start, finish);
				}
			}
		}
		
		repaint();
	}

	private int getRandomNumber(int min, int max) {
		return (int) (Math.random() * ((max - min) + 1)) + min;
	}

	private void addNode(int x, int y) {
		Node node = new Node(x, y, nodeNr);
		vectorNoduri.add(node);
		nodeNr++;
	}

	private void addArc(Point start, Point end) {
		Arc arc = new Arc(start, end);
		vectorArce.add(arc);
	}

	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		g.drawString("This is my Graph!", 10, 20);
	
		for (Arc arc : vectorArce) {
			arc.drawArc(g);
		}

		for (Node nod : vectorNoduri) {
			nod.drawNode(g, node_diam, node_diam);
		}
	}
}
