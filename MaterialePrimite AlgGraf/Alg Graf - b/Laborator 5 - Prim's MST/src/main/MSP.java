package main;
  
class MST 
{ 
    private static final int V = 5; 
  
    // Find the vertex with minimum key value, from the set of vertices not yet included in MST 
    int minKey(int key[], Boolean mstSet[]) 
    { 
        int min = Integer.MAX_VALUE, min_index=-1; 
  
        for (int v = 0; v < V; v++) 
            if (mstSet[v] == false && key[v] < min) 
            { 
                min = key[v]; 
                min_index = v; 
            } 
  
        return min_index; 
    } 
  
    // Print the constructed MST stored in parent[] 
    void printMST(int parent[], int n, int graph[][]) 
    { 
        System.out.println("Edge \tWeight"); 
        for (int i = 1; i < V; i++) 
            System.out.println(parent[i]+" - "+ i+"\t"+ 
                            graph[i][parent[i]]); 
    } 
  
    // Construct MST for a graph represented using adjacency matrix representation 
    void primMST(int graph[][]) 
    { 
        int parent[] = new int[V]; 

        int key[] = new int [V]; 
  
        Boolean mstSet[] = new Boolean[V]; 
  
        for (int i = 0; i < V; i++) 
        { 
            key[i] = Integer.MAX_VALUE; 
            mstSet[i] = false; 
        } 
   
        key[0] = 0;     
        parent[0] = -1; // root of MST 
  
        for (int count = 0; count < V-1; count++) 
        { 
            int u = minKey(key, mstSet); 
  
            mstSet[u] = true; 
   
            for (int v = 0; v < V; v++) 
                if (graph[u][v] != 0 && mstSet[v] == false && 
                    graph[u][v] < key[v]) 
                { 
                    parent[v] = u; 
                    key[v] = graph[u][v]; 
                } 
        } 
        printMST(parent, V, graph); 
    } 
}
