
public class Main {

	public static void main(String[] args) {
		
        int V = 5; 
        
        Graph g = new Graph(V); 
       
        g.addEdge(1, 0); 
        g.addEdge(0, 2); 
        g.addEdge(2, 1); 
       // g.addEdge(0, 3); 
        g.addEdge(3, 4); 
        
        System.out.println("Graph represented by adjacency list: \n");
        g.printGraph(); 

        System.out.println("Its connected components: ");
        g.connectedComponents();
        
        System.out.println("");
        
        System.out.println("Its strongly connected components: ");
        g.stronglyCnnectedComponents();
        
        System.out.println("");
        
        System.out.println("Its topological sort: ");
        g.topologicalSort();
	}

}
