package main;

import java.util.LinkedList; 
import java.util.Queue; 

public class Graph {
	
	/**
	 * @param rGraph residual graph
	 * @param s source vertex
	 * @param t sink vertex
	 * @param parent used to store the path
	 * @return true if there is a path from s to t in residual graph
	 */ 
    private static boolean BFS(int[][] rGraph, int s, int t, int[] parent) 
    {             
        boolean[] visited = new boolean[rGraph.length]; 
        for(int i = 0; i < rGraph.length; ++i)
			visited[i] = false;
            
        Queue<Integer> queue = new LinkedList<Integer>(); 
        queue.add(s); 
        visited[s] = true; 
        parent[s] = -1; 
          
        // Standard BFS Loop      
        while (!queue.isEmpty()) 
        { 
            int v = queue.poll(); 
            
            for (int i = 0; i < rGraph.length; i++) 
            { 
                if (rGraph[v][i] > 0 && !visited[i]) { 
                	queue.offer(i); 
                    visited[i] = true; 
                    parent[i] = v; 
                } 
            } 
        } 
             
        return (visited[t] == true); 
    } 
      
    /**
     * @param rGraph residual graph
     * @param s source vertex
     * @param visited
     * find all reachable vertices from s
     * mark visited[i] true if i is reachable from s
     */
    private static void DFS(int[][] rGraph, int s, boolean[] visited) 
    { 
        visited[s] = true; 
        
        for (int i = 0; i < rGraph.length; i++) 
        { 
            if (rGraph[s][i] > 0 && !visited[i]) { 
                DFS(rGraph, i, visited); 
            } 
        } 
    } 
  
    /**
     * @param graph
     * @param s source node
     * @param t sink node
     * print the minimum s-t cut
     */
    private static void minCut(int[][] graph, int s, int t) 
    { 
        int u, v; 
        
        //Create a residual graph
        int[][] rGraph = new int[graph.length][graph.length];
        
        //Initialize residual capacities in residual graph
      	//with given capacities in the original graph
        for (int i = 0; i < graph.length; i++) { 
            for (int j = 0; j < graph.length; j++) { 
                rGraph[i][j] = graph[i][j]; 
            } 
        } 
  
        //This array is filled by BFS and to store path 
        int[] parent = new int[graph.length];  
          
        //Increment the flow while there is path from s to t      
        while (BFS(rGraph, s, t, parent)) 
        { 
              
        	//Find the minimum residual capacity along the path 
            int pathFlow = Integer.MAX_VALUE;          
            for (v = t; v != s; v = parent[v]) { 
                u = parent[v]; 
                pathFlow = Math.min(pathFlow, rGraph[u][v]); 
            } 
              
            //Update residual capacities 
			//and reverse edges along the path
            for (v = t; v != s; v = parent[v]) { 
                u = parent[v]; 
                rGraph[u][v] = rGraph[u][v] - pathFlow; 
                rGraph[v][u] = rGraph[v][u] + pathFlow; 
            } 
        } 
          
        // Flow is maximum now, find vertices reachable from s      
        boolean[] isVisited = new boolean[graph.length];      
        DFS(rGraph, s, isVisited); 
          
        // Print all edges that are from a reachable vertex to 
        // non-reachable vertex in the original graph      
        for (int i = 0; i < graph.length; i++) { 
            for (int j = 0; j < graph.length; j++) { 
                if (graph[i][j] > 0 && isVisited[i] && !isVisited[j]) { 
                    System.out.println(i + " - " + j); 
                } 
            } 
        } 
    }
    
    public static void main(String[] args) {
		
		int graph[][] = { {0, 16, 13, 0, 0, 0}, 
                		  {0, 0, 10, 12, 0, 0}, 
                		  {0, 4, 0, 0, 14, 0}, 
                		  {0, 0, 9, 0, 0, 20}, 
                		  {0, 0, 0, 7, 0, 4}, 
                		  {0, 0, 0, 0, 0, 0} 
            			  }; 
        minCut(graph, 0, 5); 
	}
}
