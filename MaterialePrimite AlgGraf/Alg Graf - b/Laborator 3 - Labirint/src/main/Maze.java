package main;

public class Maze {
	private int N;

	/**
	 * Constructor
	 * 
	 * @param N
	 */
	Maze(int N) {
		this.N = N;
	}

	/**
	 * Prints solution matrix.
	 * 
	 * @param sol
	 * @return void
	 */
	void printSolution(int sol[][]) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++)
				System.out.print(" " + sol[i][j] + " ");
			System.out.println();
		}
	}

	/**
	 * Checks if x,y is valid index for the maze
	 * 
	 * @param maze
	 * @param x
	 * @param y
	 * @return true if position is safe, false otherwise
	 */
	boolean isSafe(int maze[][], int x, int y) {
		// if (x,y outside maze) return false
		return (x >= 0 && x < N && y >= 0 && y < N && maze[x][y] == 1);
	}

	/**
	 * Checks if there is path. Prints the path (if found).
	 * 
	 * @param maze
	 * @return false if no path is possible, true otherwise
	 */
	boolean solveMaze(int maze[][]) {
		int sol[][] = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } };

		if (solveMazeUtil(maze, 0, 0, sol) == false) {
			System.out.print("Solution doesn't exist");
			return false;
		}

		printSolution(sol);
		return true;
	}

	/**
	 * Solves the maze recursively.
	 * 
	 * @param maze
	 * @param x
	 * @param y
	 * @param sol
	 * @return
	 */
	boolean solveMazeUtil(int maze[][], int x, int y, int sol[][]) {

		// if (x,y is goal) return true
		if (x == N - 1 && y == N - 1) {
			sol[x][y] = 1;
			return true;
		}

		// Check if maze[x][y] is valid
		if (isSafe(maze, x, y) == true) {
			// mark x,y as part of solution path
			sol[x][y] = 1;

			/* Move forward in x direction */
			if (solveMazeUtil(maze, x + 1, y, sol))
				return true;

			/*
			 * If moving in x direction doesn't give solution then Move down in y direction
			 */
			if (solveMazeUtil(maze, x, y + 1, sol))
				return true;

			/*
			 * If none of the above movements works then BACKTRACK: unmark x,y as part of
			 * solution path
			 */
			sol[x][y] = 0;
			return false;
		}

		return false;
	}
}
