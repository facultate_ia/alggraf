
package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;


public class View extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
     * Conventions:
     * 
     * maze[row][column]
     * 
     * Values: 0 = not-visited node
     *         1 = wall (blocked)
     *         2 = start
     *         3 = end
     *         4 = visited node
     *
     */
	
    private int [][] maze = 
        {  	{ 1, 2, 1, 1, 1, 1, 1, 1 },
            { 1, 0, 0, 0, 0, 0, 0, 1 },
            { 1, 0, 1, 1, 1, 1, 0, 1 },
            { 1, 0, 0, 1, 1, 0, 0, 1 },
            { 1, 0, 0, 0, 0, 0, 1, 1 },
            { 1, 1, 1, 0, 0, 0, 1, 1 },
            { 3, 0, 0, 0, 0, 0, 0, 3 },
            { 1, 1, 1, 3, 1, 1, 1, 1 }
        };
    
    private final List<Integer> path = new ArrayList<Integer>();
    private int pathIndex;
    
    private int rowStart;
    private int colStart;
  
    public View() {
        
    	setTitle("Maze Solver");
        setSize(640, 480);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //calculate start position
        for (int row = 0; row < maze.length; row++) {
        	for (int col = 0; col < maze[0].length; col++) {
        		if(maze[row][col] == 2) {
                	rowStart = row;
                	colStart = col;
                }
            }
        }
        
        DepthFirst.searchPath(maze, rowStart, colStart, path);
        pathIndex = path.size() - 2;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        
        g.translate(50, 50);
        
        // draw the maze
        for (int row = 0; row < maze.length; row++) {
            for (int col = 0; col < maze[0].length; col++) {
                Color color;
                switch (maze[row][col]) {
                    case 1 : color = Color.BLACK; break;
                    case 2 : color = Color.RED; break;
                    case 3 : color = Color.BLUE;break;
                    default : color = Color.WHITE;
                }
                g.setColor(color);
                g.fillRect(30 * col, 30 * row, 30, 30);
                g.setColor(Color.BLACK);
                g.drawRect(30 * col, 30 * row, 30, 30);
            }
        }
        
        // draw the path list
        for (int p = 0; p < path.size(); p += 2) {
            int pathX = path.get(p);
            int pathY = path.get(p + 1);
            g.setColor(Color.GREEN);
            g.fillRect(pathX * 30, pathY * 30, 30, 30);
        }
        
        // draw the ball on path
        int pathX = path.get(pathIndex);
        int pathY = path.get(pathIndex + 1);
        g.setColor(Color.RED);
        g.fillOval(pathX * 30, pathY * 30, 30, 30);
    }
    
    //make the ball move through maze
    //using left and right keys
    
    @Override
    protected void processKeyEvent(KeyEvent ke) {
        if (ke.getID() != KeyEvent.KEY_PRESSED) {
            return;
        }
        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            pathIndex -= 2;
            if (pathIndex < 0) {
                pathIndex = 0;
            }
        }
        else if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            pathIndex += 2;
            if (pathIndex > path.size() - 2) {
                pathIndex = path.size() - 2;
            }
        }
        repaint(); 
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                View view = new View();
                view.setVisible(true);
            }
        });
    }
    
}
