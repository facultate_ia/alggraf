package main;

public class Main {

	public static void main(String[] args) {
		
		int graph[][] = new int[][] { {0, 16, 13, 0, 0, 0}, 
            						  {0, 0, 10, 12, 0, 0}, 
            						  {0, 4, 0, 0, 14, 0}, 
            						  {0, 0, 9, 0, 0, 20}, 
            						  {0, 0, 0, 7, 0, 4}, 
            						  {0, 0, 0, 0, 0, 0} 
          							};
       int s = 0;
       int t = 5;
       
       MaxFlow maxFlow = new MaxFlow(6);
        
       System.out.println(" Maximum flow for the graph: " + 
        					maxFlow.fordFulkerson(graph, s, t));
	}

}
