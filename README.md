# AlgGraf Teme
## Astea sunt notele pe care le-am luat eu

1. **[Desenare graf](src/Tema1) - 10**
2. **[Labirint](src/Tema2) - 10**
3. **[Sortare topologica + Componente conexe + Componente tare conexe](src/Tema3) - 10**
4. **[Algoritmul lui Prim](src/Tema4) - 10**
5. **[Luxemburg (Dijkstra)](src/Tema5) - 5**
6. **[Flux maxim](src/Tema6) - 10**
7. **[Flux minim](src/Tema7) - 5**

- [CS.pdf](CS.pdf) e cartea pentru prima parte, adica grafuri.
- [AOC.pdf](AOC.pdf) e cartea pentru a doua parte, adica fluxuri.
- [MaterialePrimite AlgGraf](MaterialePrimite AlgGraf) e un folder cu materiale pe care le-am primit si eu de la colegi mai mari.